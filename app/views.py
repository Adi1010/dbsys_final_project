#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import render_template, flash, redirect, request
from app import app
import json
import queries


@app.route("/")
def main():

    language_data = []
    top_language_data = []
    words_data = []
    words_data_gauge = []
    country_data = []


    lang, top_lang, love_words, swear_words, country, datetime = queries.read_data()
    for l in lang:
        language_data.append([l[0], l[1], l[1]])

    for t in top_lang:
        top_language_data.append([t[0], t[1], t[1]])

    words_data.append(['love_words', love_words, love_words])
    words_data.append(['swear_words', swear_words, swear_words])

    love_words_percent = int((love_words * 100) / (love_words + swear_words))
    swear_words_percent = int((swear_words * 100) / (love_words + swear_words))

    words_data_gauge.append(['Label', 'Value'])
    words_data_gauge.append(['love_words', love_words_percent])
    words_data_gauge.append(['swear_words', swear_words_percent])

    country_data.append(['Country', 'Popularity'])

    for coun in country:
        country_data.append([coun[0], coun[1]])

    return render_template('analytics1.html', language_data = language_data, top_language_data = top_language_data,  words_data = words_data, \
                            words_data_gauge = words_data_gauge, country_data = country_data, datetime = datetime)



@app.route("/top_tweets")
def top_tweets():
    tweets, datetime_toptweets = queries.get_top_tweets()
    return render_template('top_tweets1.html', tweets = tweets, datetime_toptweets = datetime_toptweets)



@app.route("/trends")
def trends():
    trend, trend_tweet, datetime_trends = queries.get_trends()
    return render_template('trends1.html', trend = trend, trend_tweet = trend_tweet, datetime_trends = datetime_trends)


