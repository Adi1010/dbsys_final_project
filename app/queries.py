import sqlite3
import ast


def get_top_tweets():
    conn = sqlite3.connect("./twit_data.db")
    conn.row_factory = sqlite3.Row
    c = conn.cursor()

    c.execute("SELECT * from twit_data  ORDER BY datetime DESC LIMIT 30")
    result = c.fetchall()
    tweets = []

    datetime_toptweets = result[0]['datetime']

    for tweet in result:
        tweets.append(tweet['top_tweet'])

    conn.close()

    return tweets, datetime_toptweets

def get_trends():
    conn = sqlite3.connect("./twit_data.db")
    conn.row_factory = sqlite3.Row
    c = conn.cursor()

    trend = []
    trend_tweet = []

    c.execute("SELECT * from trend_data ORDER BY datetime DESC LIMIT 10")
    result = c.fetchall()

    datetime_trends = result[0]['datetime']

    for r in result:
        trend.append(r['trend'])
        trend_tweet.append(r['trend_id1'])
        trend_tweet.append(r['trend_id2'])
        trend_tweet.append(r['trend_id3'])

    conn.close()

    return trend, trend_tweet, datetime_trends

def read_data():

    conn = sqlite3.connect("./twit_data.db")
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("SELECT * from lang_data ORDER BY datetime DESC LIMIT 1")

    result = c.fetchone()
    lang = ast.literal_eval(result['language'])
    top_lang = ast.literal_eval(result['top_language'])

    c.execute("SELECT * from love_data ORDER BY datetime DESC LIMIT 1")

    result = c.fetchone()
    love_words = result['love_words']
    swear_words = result['swear_words']

    c.execute("SELECT * from country_data ORDER BY datetime DESC LIMIT 1")

    result = c.fetchone()
    country = ast.literal_eval(result['country'])
    datetime = result['datetime']

    conn.close()

    return lang, top_lang, love_words, swear_words, country, datetime