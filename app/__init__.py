from flask import Flask, render_template
from logging.handlers import RotatingFileHandler
import logging
import os
import json

app = Flask(__name__)


formatter = logging.Formatter(
    "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
logs_path = os.environ.get('LOGS_PATH', os.path.join(os.getcwd(),'logs'))
if not os.path.exists(logs_path): # TODO: make log folder path configurable by env variable
    os.mkdir(logs_path)
handler = RotatingFileHandler(str(logs_path)+"/twitter_analysis.log", maxBytes=10000000, backupCount=5)
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.logger.addHandler(handler)


import views